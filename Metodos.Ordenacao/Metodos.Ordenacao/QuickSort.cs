﻿namespace Metodos.Ordenacao
{
    /// <summary>
    /// Classe responsável pela ordenação utilizando as técnicas de Quick Sort
    /// </summary>
    public class QuickSort
    {
        /// <summary>
        /// Ordena um vetor de números inteiros utilizando o algoritmo de Insertion Sort
        /// </summary>
        /// <param name="vetor">Vetor que será ordenado</param>
        /// <returns>Vetor ordenado</returns>
        public static void Ordernar(int[] vetor, int primeiro, int ultimo)
        {
            int baixo, alto, meio, pivo, repositorio;
            baixo = primeiro;
            alto = ultimo;
            meio = (baixo + alto) / 2;

            pivo = vetor[meio];

            while (baixo <= alto)
            {
                while (vetor[baixo] < pivo)
                    baixo++;

                while (vetor[alto] > pivo)
                    alto--;

                if (baixo < alto)
                {
                    repositorio = vetor[baixo];
                    vetor[baixo++] = vetor[alto];
                    vetor[alto--] = repositorio;
                }
                else
                {
                    if (baixo == alto)
                    {
                        baixo++;
                        alto--;
                    }
                }
            }

            if (alto > primeiro)
                Ordernar(vetor, primeiro, alto);

            if (baixo < ultimo)
                Ordernar(vetor, baixo, ultimo);
        }
    }
}