﻿using System;
using System.Diagnostics;

namespace Metodos.Ordenacao
{
    class Program
    {
        //Constante que armazena o tamanho dos vetores que serão criados para serem processados.
        //Para fins de teste em sala de aula é melhor utilizarmos um valor menor para conseguir enxergar as informações geradas.
        private const int _tamanhoVetor = 1000;

        static void Main(string[] args)
        {
            //Objeto do .NET Framework que tem a função de determinar o tempo de execução de um processo
            Stopwatch tempoProcessamento = new Stopwatch();
            
            //Criação dos vetores solicitados pelo exercício
            int[] vetorA = CriarVetorA();
            int[] copiaVetorA = new int[_tamanhoVetor];

            int[] vetorB = CriarVetorB();
            int[] copiaVetorB = new int[_tamanhoVetor];

            int[] vetorC = CriarVetorC();
            int[] copiaVetorC = new int[_tamanhoVetor];

            //Copiamos os vetores para a cada execução da ordenação utilizarmos as cópias dos vetores para "desordena-los" novamente
            Array.Copy(vetorA, copiaVetorA, vetorA.Length);
            Array.Copy(vetorB, copiaVetorB, vetorB.Length);
            Array.Copy(vetorC, copiaVetorC, vetorC.Length);

            //Bubble Sort Vetor A

            //Início do cronômetro
            tempoProcessamento.Start();
            //Chamada do método de ordenação Bubble Sort para o vetor A
            BubbleSort.Ordernar(vetorA);
            //Parada do cronômetro
            tempoProcessamento.Stop();
            //Chamada do método que mostra as informações da ordenação do vetor
            MostrarResultado("Bubble Sort Vetor A", tempoProcessamento.Elapsed, copiaVetorA, vetorA);
            //Fazemos nosso vetorA ficar igual a copiarVetorA para "desordena-lo"
            Array.Copy(copiaVetorA, vetorA, copiaVetorA.Length);

            Console.WriteLine("Pressione qualquer tecla para continuar...");
            Console.ReadKey();
            Console.WriteLine("\nProcessando, por favor aguarde...\n");

            //Bubble Sort Vetor B
            tempoProcessamento.Start();
            BubbleSort.Ordernar(vetorB);
            tempoProcessamento.Stop();
            MostrarResultado("Bubble Sort Vetor B", tempoProcessamento.Elapsed, copiaVetorB, vetorB);
            Array.Copy(copiaVetorB, vetorB, copiaVetorB.Length);

            Console.WriteLine("Pressione qualquer tecla para continuar...");
            Console.ReadKey();
            Console.WriteLine("\nProcessando, por favor aguarde...\n");

            //Bubble Sort Vetor C
            tempoProcessamento.Start();
            BubbleSort.Ordernar(vetorC);
            tempoProcessamento.Stop();
            MostrarResultado("Bubble Sort Vetor C", tempoProcessamento.Elapsed, copiaVetorC, vetorC);
            Array.Copy(copiaVetorC, vetorC, copiaVetorC.Length);

            Console.WriteLine("Pressione qualquer tecla para continuar...");
            Console.ReadKey();
            Console.WriteLine("\nProcessando, por favor aguarde...\n");

            //Selection Sort Vetor A
            tempoProcessamento.Restart();
            SelectionSort.Ordernar(vetorA);
            tempoProcessamento.Stop();
            MostrarResultado("Selection Sort Vetor A", tempoProcessamento.Elapsed, copiaVetorA, vetorA);
            Array.Copy(copiaVetorA, vetorA, copiaVetorA.Length);

            Console.WriteLine("Pressione qualquer tecla para continuar...");
            Console.ReadKey();
            Console.WriteLine("\nProcessando, por favor aguarde...\n");

            //Selection Sort Vetor B
            tempoProcessamento.Restart();
            SelectionSort.Ordernar(vetorB);
            tempoProcessamento.Stop();
            MostrarResultado("Selection Sort Vetor B", tempoProcessamento.Elapsed, copiaVetorB, vetorB);
            Array.Copy(copiaVetorB, vetorB, copiaVetorB.Length);

            Console.WriteLine("Pressione qualquer tecla para continuar...");
            Console.ReadKey();
            Console.WriteLine("\nProcessando, por favor aguarde...\n");

            //Selection Sort Vetor C
            tempoProcessamento.Restart();
            SelectionSort.Ordernar(vetorC);
            tempoProcessamento.Stop();
            MostrarResultado("Selection Sort Vetor C", tempoProcessamento.Elapsed, copiaVetorC, vetorC);
            Array.Copy(copiaVetorC, vetorC, copiaVetorC.Length);

            Console.WriteLine("Pressione qualquer tecla para continuar...");
            Console.ReadKey();
            Console.WriteLine("\nProcessando, por favor aguarde...\n");

            //Insertion Sort Vetor A
            tempoProcessamento.Restart();
            InsertionSort.Ordernar(vetorA);
            tempoProcessamento.Stop();
            Array.Copy(copiaVetorA, vetorA, copiaVetorA.Length);
            MostrarResultado("Insertion Sort Vetor A", tempoProcessamento.Elapsed, copiaVetorA, vetorA);

            Console.WriteLine("Pressione qualquer tecla para continuar...");
            Console.ReadKey();
            Console.WriteLine("\nProcessando, por favor aguarde...\n");

            //Insertion Sort Vetor B
            tempoProcessamento.Restart();
            InsertionSort.Ordernar(vetorB);
            tempoProcessamento.Stop();
            Array.Copy(copiaVetorB, vetorB, copiaVetorB.Length);
            MostrarResultado("Insertion Sort Vetor B", tempoProcessamento.Elapsed, copiaVetorB, vetorB);

            Console.WriteLine("Pressione qualquer tecla para continuar...");
            Console.ReadKey();
            Console.WriteLine("\nProcessando, por favor aguarde...\n");

            //Insertion Sort Vetor C
            tempoProcessamento.Restart();
            InsertionSort.Ordernar(vetorC);
            tempoProcessamento.Stop();
            Array.Copy(copiaVetorA, vetorC, copiaVetorC.Length);
            MostrarResultado("Insertion Sort Vetor C", tempoProcessamento.Elapsed, copiaVetorC, vetorC);

            Console.WriteLine("Pressione qualquer tecla para continuar...");
            Console.ReadKey();
            Console.WriteLine("\nProcessando, por favor aguarde...\n");

            //Quick Sort Vetor A
            tempoProcessamento.Restart();
            QuickSort.Ordernar(vetorA, 0, vetorA.Length - 1);
            tempoProcessamento.Stop();
            MostrarResultado("Quick Sort Vetor A", tempoProcessamento.Elapsed, copiaVetorA, vetorA);
            Array.Copy(copiaVetorA, vetorA, copiaVetorA.Length);

            Console.WriteLine("Pressione qualquer tecla para continuar...");
            Console.ReadKey();
            Console.WriteLine("\nProcessando, por favor aguarde...\n");

            //Quick Sort Vetor B
            tempoProcessamento.Restart();
            QuickSort.Ordernar(vetorB, 0, vetorB.Length - 1);
            tempoProcessamento.Stop();
            MostrarResultado("Quick Sort Vetor B", tempoProcessamento.Elapsed, copiaVetorB, vetorB);
            Array.Copy(copiaVetorB, vetorB, copiaVetorB.Length);

            Console.WriteLine("Pressione qualquer tecla para continuar...");
            Console.ReadKey();
            Console.WriteLine("\nProcessando, por favor aguarde...\n");

            //Quick Sort Vetor C
            tempoProcessamento.Restart();
            QuickSort.Ordernar(vetorC, 0, vetorC.Length - 1);
            tempoProcessamento.Stop();
            MostrarResultado("Quick Sort Vetor C", tempoProcessamento.Elapsed, copiaVetorC, vetorC);
            Array.Copy(copiaVetorC, vetorC, copiaVetorC.Length);

            Console.WriteLine("Pressione qualquer tecla para continuar...");
            Console.ReadKey();
            Console.WriteLine("\nProcessando, por favor aguarde...\n");

            //Merge Sort Vetor A
            tempoProcessamento.Restart();
            vetorA = MergeSort.Ordernar(vetorA, 0, vetorA.Length);
            tempoProcessamento.Stop();
            MostrarResultado("Merge Sort Vetor A", tempoProcessamento.Elapsed, copiaVetorA, vetorA);
            Array.Copy(copiaVetorA, vetorA, copiaVetorA.Length);

            Console.WriteLine("Pressione qualquer tecla para continuar...");
            Console.ReadKey();
            Console.WriteLine("\nProcessando, por favor aguarde...\n");

            //Merge Sort Vetor B
            tempoProcessamento.Restart();
            vetorA = MergeSort.Ordernar(vetorB, 0, vetorB.Length);
            tempoProcessamento.Stop();
            MostrarResultado("Merge Sort Vetor B", tempoProcessamento.Elapsed, copiaVetorB, vetorB);
            Array.Copy(copiaVetorB, vetorB, copiaVetorB.Length);

            Console.WriteLine("Pressione qualquer tecla para continuar...");
            Console.ReadKey();
            Console.WriteLine("\nProcessando, por favor aguarde...\n");

            //Merge Sort Vetor C
            tempoProcessamento.Restart();
            vetorA = MergeSort.Ordernar(vetorC, 0, vetorC.Length);
            tempoProcessamento.Stop();
            MostrarResultado("Merge Sort Vetor C", tempoProcessamento.Elapsed, copiaVetorC, vetorC);
            Array.Copy(copiaVetorC, vetorC, copiaVetorC.Length);

            Console.WriteLine("Pressione qualquer tecla para finalizar o programa...");
            Console.ReadKey();
        }

        /// <summary>
        /// Cria o vetor A com o tamanho da constante _tamanhoVetor
        /// </summary>
        /// <returns>Vetor A criado</returns>
        static int[] CriarVetorA()
        {
            //Criamos um vetor inteiro do tamanho da constante
            int[] vetor = new int[_tamanhoVetor];

            //Populamos o vetor
            for (int i = 0; i < _tamanhoVetor; i++)
            {
                //Se for o primeira iteração
                if (i == 0)
                    vetor[i] = i + 1;
                //Se o número for par
                else if (i % 2 == 0)
                    vetor[i] = i;
                else
                    //Se o número for ímpar
                    vetor[i] = i + 2;
            }

            return vetor;
        }

        /// <summary>
        /// Cria o vetor B com o tamanho da constante _tamanhoVetor
        /// </summary>
        /// <returns>Vetor B criado</returns>
        static int[] CriarVetorB()
        {
            //Criamos um vetor inteiro do tamanho da constante
            int[] vetor = new int[_tamanhoVetor];

            //Populamos o vetor
            for (int i = 0; i < _tamanhoVetor; i++)
            {
                vetor[i] = _tamanhoVetor - i;
            }

            return vetor;
        }

        /// <summary>
        /// Cria o vetor C com o tamanho da constante _tamanhoVetor
        /// </summary>
        /// <returns>Vetor C criado</returns>
        static int[] CriarVetorC()
        {
            //Instância do objeto Random, que tem a função de criar números aleatórios
            Random random = new Random();
            //Criamos um vetor inteiro do tamanho da constante
            int[] vetor = new int[_tamanhoVetor];

            //Populamos o vetor
            for (int i = 0; i < _tamanhoVetor; i++)
            {
                //A função Next() gera um número aleátorio que pode ser até no máximo o valor que defini como parâmetro, nesse caso o valor aleatório máximo que pode
                //ser gerado é o valor da constante _tamanhoVetor
                vetor[i] = random.Next(_tamanhoVetor);
            }

            return vetor;
        }

        /// <summary>
        /// Lista informações sobre a execução do algoritmo de ordenação
        /// </summary>
        /// <param name="nomeAlgoritmo">Nome do algoritmo de ordenação que vai ser apresentado</param>
        /// <param name="tempoProcessamento">Tempo de processamento gasto</param>
        /// <param name="vetorOriginal">Vetor original (desordenado)</param>
        /// <param name="vetorOrdenado">Vetor após a ordenação</param>
        static void MostrarResultado(string nomeAlgoritmo, TimeSpan tempoProcessamento, int[] vetorOriginal, int[] vetorOrdenado)
        {
            Console.WriteLine(string.Format("-----------------> {0} <-----------------\n", nomeAlgoritmo));
            Console.WriteLine(string.Format("--> Vetor original: {0}", string.Join(", ", vetorOriginal)));
            Console.WriteLine(string.Format("--> Vetor ordenado: {0}\n\n", string.Join(", ", vetorOrdenado)));
            Console.WriteLine(string.Format("--> Tempo de processamento do algoritmo: {0}\n", tempoProcessamento.ToString(@"hh\:mm\:ss\:ffff")));
        }
    }
}