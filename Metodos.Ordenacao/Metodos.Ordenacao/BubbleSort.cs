﻿namespace Metodos.Ordenacao
{
    /// <summary>
    /// Classe responsável pelas técnicas de ordenação Bubble Sort
    /// </summary>
    public class BubbleSort
    {
        /// <summary>
        /// Ordena um vetor de números inteiros utilizando o algoritmo de Bubble Sort
        /// </summary>
        /// <param name="vetor">Vetor que será ordenado</param>
        /// <returns>Vetor ordenado</returns>
        public static int[] Ordernar(int[] vetor)
        {
            //Variável auxiliar que irá armazenar temporariamente um valor do vetor
            int numeroAuxiliar = 0;
            //Variável para informar se houve alguma troca no algoritmo, caso não houver troca significa que o vetor já está ordenado, então não precisamos continuar o processamento.
            bool trocou = false;

            // i determina o número de etapas para a ordenação
            for (int i = 0; i < vetor.Length - 1; i++) 
            {
                trocou = false;

                // j determina o número de comparações em cada etapa e os índices a serem pesquisados para a comparação. 
                for (int j = 0; j < vetor.Length - (i + 1); j++)
                {
                    if (vetor[j] > vetor[j + 1])
                    {
                        trocou = true;
                        numeroAuxiliar = vetor[j];
                        vetor[j] = vetor[j + 1];
                        vetor[j + 1] = numeroAuxiliar;
                    }
                }

                //Se não houve troca podemos parar a execução do algoritmo de ordenação para evitarmos o processamento desnecessário
                if (!trocou)
                    break;
            }

            return vetor;
        }
    }
}