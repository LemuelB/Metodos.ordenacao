﻿namespace Metodos.Ordenacao
{
    /// <summary>
    /// Classe responsável pela ordenação utilizando as técnicas de Insertion Sort
    /// </summary>
    public class InsertionSort
    {
        /// <summary>
        /// Ordena um vetor de números inteiros utilizando o algoritmo de Insertion Sort
        /// </summary>
        /// <param name="vetor">Vetor que será ordenado</param>
        /// <returns>Vetor ordenado</returns>
        public static int[] Ordernar(int[] vetor)
        {
            //Variável para armazenar o número auxiliar
            int numeroAuxiliar;
            int j;

            for (int i = 1; i < vetor.Length; i++)
            {
                j = i - 1;

                while (j >= 0 && vetor[j] > vetor[j + 1])
                {
                    numeroAuxiliar = vetor[j];
                    vetor[j] = vetor[j + 1];
                    vetor[j + 1] = numeroAuxiliar;

                    j--;
                }
            }

            return vetor;
        }
    }
}