﻿namespace Metodos.Ordenacao
{
    /// <summary>
    /// Classe responsável pela ordenação utilizando as técnicas de Selection Sort
    /// </summary>
    public class SelectionSort
    {
        /// <summary>
        /// Ordena um vetor de números inteiros utilizando o algoritmo de Selection Sort
        /// </summary>
        /// <param name="vetor">Vetor que será ordenado</param>
        /// <returns>Vetor ordenado</returns>
        public static int[] Ordernar(int[] vetor)
        {
            //Variável para armazenar o menor número encontrado em cada iteração
            int menorValor;
            //Variável para armazenar o número auxiliar
            int numeroAuxiliar = 0;

            for (int i = 0; i < vetor.Length - 1; i++)
            {
                menorValor = i;

                for (int j = i + 1; j < vetor.Length; j++)
                {
                    if (vetor[j] < vetor[menorValor])
                    {
                        menorValor = j;
                    }
                }

                if (menorValor != i)
                {
                    numeroAuxiliar = vetor[menorValor];
                    vetor[menorValor] = vetor[i];
                    vetor[i] = numeroAuxiliar;
                }
            }

            return vetor;
        }
    }
}