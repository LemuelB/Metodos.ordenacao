﻿namespace Metodos.Ordenacao
{
    /// <summary>
    /// Classe responsável pela ordenação utilizando as técnicas de Merge Sort
    /// </summary>
    public class MergeSort
    {
        /// <summary>
        /// Ordena um vetor de números inteiros utilizando o algoritmo de Insertion Sort
        /// </summary>
        /// <param name="vetor">Vetor que será ordenado</param>
        /// <returns>Vetor ordenado</returns>
        public static int[] Ordernar(int[] vetor, int inicio, int fim)
        {
            if (fim - inicio < 2)
                return new int[] { vetor[inicio] };

            int meio = inicio + ((fim - inicio) / 2);
            int[] esquerda = Ordernar(vetor, inicio, meio);
            int[] direita = Ordernar(vetor, meio, fim);

            //Ordenar e mesclar
            int[] resultado = new int[esquerda.Length + direita.Length];
            int indiceEsquerda = 0;
            int indiceDireita = 0;
            int i = 0;

            for (; indiceEsquerda < esquerda.Length && indiceDireita < direita.Length; i++)
            {
                if (esquerda[indiceEsquerda] < direita[indiceDireita])
                {
                    resultado[i] = esquerda[indiceEsquerda];
                    indiceEsquerda++;
                }
                else
                {
                    resultado[i] = direita[indiceDireita];
                    indiceDireita++;
                }
            }

            //Copiar os elementos restantes
            while (indiceEsquerda < esquerda.Length)
                resultado[i++] = esquerda[indiceEsquerda++];

            while (indiceDireita < direita.Length)
                resultado[i++] = direita[indiceDireita++];

            return resultado;
        }
    }
}